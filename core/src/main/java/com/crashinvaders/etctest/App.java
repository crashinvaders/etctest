package com.crashinvaders.etctest;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ExtendViewport;


public class App extends ApplicationAdapter {
    private Stage stage;
    private Skin skin;

    private Image imgTexture;
    private Texture texture;

    @Override
    public void create() {
        stage = new Stage(new ExtendViewport(640, 480));
        stage.addListener(new DebugInputHandler());
        Gdx.input.setInputProcessor(stage);

        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));

        Table rootTable = new Table();
        rootTable.setFillParent(true);


        final SelectBox<String> cbTextures = new SelectBox<>(skin);
        cbTextures.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onTextureSelected(cbTextures.getSelected());
            }
        });
        rootTable.add(cbTextures).growX();

        rootTable.row().padTop(8f);

        imgTexture = new Image();
        imgTexture.setScaling(Scaling.fit);
        rootTable.add(imgTexture).grow();

        stage.addActor(rootTable);

        cbTextures.setItems(
                "original.png",
                "etc1.ktx",
                "etc1.zktx",
                "etc1a.ktx",
                "etc1a.zktx",
                "etc2-RGB8.ktx",
                "etc2-RGB8.zktx",
                "etc2-RGB8A1.ktx",
                "etc2-RGB8A1.zktx",
                "etc2-RGBA8.ktx",
                "etc2-RGBA8.zktx",
                "etc2-SRGB8.ktx",
                "etc2-SRGB8.zktx",
                "etc2-SRGB8A1.ktx",
                "etc2-SRGB8A1.zktx",
                "etc2-R11.ktx",
                "etc2-R11.zktx"
        );
    }

    @Override
    public void dispose() {
        stage.dispose();
        skin.dispose();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();
    }

    private void onTextureSelected(String textureName) {
        if (texture != null) {
            imgTexture.setDrawable(null);
            texture.dispose();
            texture = null;
        }

        try {
            texture = new Texture(Gdx.files.internal(textureName));
            imgTexture.setDrawable(new TextureRegionDrawable(new TextureRegion(texture)));
        } catch (Exception e) {
            Gdx.app.error("App", "Can't show texture: " + textureName, e);

            Label lblError = new Label("ERROR, READ OUTPUT", skin);
            lblError.setColor(new Color(0xff8040ff));
            lblError.setFontScale(2f);
            lblError.pack();
            lblError.setPosition(stage.getWidth() * 0.5f, 0f, Align.center);
            lblError.addAction(Actions.sequence(
                    Actions.parallel(
                            Actions.fadeOut(2f, Interpolation.pow5In),
                            Actions.moveBy(0f, 100f, 2f, Interpolation.pow5Out)),
                    Actions.removeActor()
            ));
            stage.addActor(lblError);
        }
    }

    private class DebugInputHandler extends InputListener {
        @Override
        public boolean keyDown(InputEvent event, int keycode) {
            switch (keycode) {
                case Input.Keys.F8: {
                    dispose();
                    create();
                    resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                    return true;
                }
            }
            return false;
        }
    }
}